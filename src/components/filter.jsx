import React, { useState } from "react";
//import PropTypes from "prop-types";
import Select from "@material-ui/core/Select";
import {
    getChildren,
    getParent
    
} from "../services/filterService.js";

function Filter({ options, changeFilters }) {
    const filterStyle = {
        marginRight: "10px"
    };

    const [value, setValue] = useState("all");

    function handleChange(event) {
        const value = event.target.value;
        console.log(`${value} selected`);
        setValue(value);
        changeFilters(value);
    }
    return (
        <div style={filterStyle}>
            <Select native onChange={handleChange}>
                {options.map(t => (
                    <option value={t} id={t}>
                        {t}
                    </option>
                ))}
            </Select>
        </div>
    );
}

Filter.defaultProps = {
    options: ["sale", "service", "jobs"]
};

Filter.propTypes = {};

function Filters({ filters, selectedFilter }) {
    const [filterKeys, setFilterKeys] = useState(filters);

    function changeFilters(value) {
        let localKeys = [...filterKeys];
        console.log(localKeys);
        let tempArray =[];
        let pv = value;
        while(pv != undefined) {
            pv = getParent(pv);
            if(pv !== null)
            {
                tempArray.push(pv);
            }
        }
        setFilterKeys([...tempArray,value]);
    }
    const filterStyle = {
        display: "flex",
        margin: "10px"
    };

    return (
        <div style={filterStyle}>
            {filterKeys.map(k => (
                <div>
                    <Filter
                        options={getChildren(k)}
                        changeFilters={changeFilters}
                    />
                </div>
            ))}
        </div>
    );
}

Filters.defaultProps = {
    selectedFilter: "abc",
    filters: ["rootnode"]
};

Filters.propTypes = {};

export { Filters, Filter} ;
