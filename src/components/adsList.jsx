import React from "react";
import PropTypes from "prop-types";
import Ad from "./ad.jsx";
import { Map } from "../components/leafletMap";

const listContainerStyle = {
    display:"flex",
    direction:"row"
};
const mapContainerStyle = {
    position:"fixed",
    left:"50%"
}
function AdsList(props) {
    const {adsData,viewType} = props;
    return (
        <div className="col-sm-12" style={listContainerStyle}>
            <div className="col-sm-6">
            {adsData.map(data => (
                <Ad adData={data} adViewType={viewType} />
            ))}
            </div>
            <div className="col-sm-6" style={mapContainerStyle}>
            <Map/>
            </div>
        </div>
    );
}

AdsList.defaultProps = {};

AdsList.propTypes = {};

export default AdsList;
