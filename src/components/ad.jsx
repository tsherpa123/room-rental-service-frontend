import { useState } from "react";
//import { Map } from "../components/leafletMap";
//import { Card} from "react-bootstrap";
import Card from "@material-ui/core/Card";
//import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

import DisplayPics from "./displayPics";
import RoomHandle from "./roomHandle";

import React from "react";
import PropTypes from "prop-types";

import "./adsCart.css";
function Ad(props) {
    const [adData, setAdData] = useState(props.adData);
    const { adViewType } = props;
    const { pictures } = adData;

    const localStyle =
        adViewType === "card"
            ? {
                  backgroundColor: "cornflowerblue",
                  width: "400px",
                  margin: "10px"
              }
            : {};

    const cartContentStyle = {
        margin: "0px",
        padding: "0px"
    };

    return (
        <Card variant="outlined" style={localStyle}>
            <CardContent style={cartContentStyle}>
                <DisplayPics pictures={pictures} />
                <Typography gutterBottom variant="h5" component="h2">
                    {adData.title}
                </Typography>
                <RoomHandle
                    date={adData.date}
                    title={adData.title}
                    price={adData.price}
                />
            </CardContent>
        </Card>
    );
}

Ad.defaultProps = {};

Ad.propTypes = {};

export default Ad;
