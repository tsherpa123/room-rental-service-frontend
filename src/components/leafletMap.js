// thiss file describe map
import React from "react";
import L from "leaflet";

function Map({ markersData }) {
    //create map
    const mapDimention ={
        height : "900px",
        width : "100%"
    }

    const cornerOne = L.latLng(49.104431, -122.8010),
        cornerTwo = L.latLng(49.104431, -122.8010);

    const mapRef = React.useRef(null);
    React.useEffect(() => {
        const container = L.DomUtil.get('map')
        if(container!= null){
           container._leaflet_id = null;  
        }

        mapRef.current = L.map("map", {
            center: [49.104431, -122.8010],
            zoom: 20,
            layers: [
                L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
                    attribution:
                        '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                }),

                L.marker([49.104431,-122.8010],{
                    title : "Pasang House",
                    draggable :true,
                    autoPan : true
                }),
                L.marker([49.104431,-122.8015],{
                    title : "Pasang House",
                    draggable :true,
                    autoPan : true
                })

            ]
        });
    }, []);
    // add layer
    const layerRef = React.useRef(null);
    React.useEffect(() => {
        layerRef.current = L.layerGroup().addTo(mapRef.current);
    }, []);
    const mapContainer = document.getElementById("map");
    if(!mapContainer){
        return <div id="map" style={mapDimention}/>;
    }
    mapContainer.parentElement.removeChild(mapContainer)

}
export {Map};
