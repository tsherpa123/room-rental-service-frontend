import React from "react";
import { useState, useEffect } from "react";
import Select from "@material-ui/core/Select";
import Pagination from "@material-ui/lab/Pagination";
import TextField from '@material-ui/core/TextField';
import {  getXAds } from "../services/adService.js";
import AdsCart from "./adsCart.jsx";
import AdsList from "./adsList.jsx";

const DisplayAllAds = props => {
    const [adsData, setAdsData] = useState([]);
    const [viewType, setViewType] = useState("card");
    const [pageNumber, setPage] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [totalItems, setTotalItems] = useState(0);
    const pageSize = 10;

    useEffect(() => {
        console.log("use effect called");
        getXAds(1, pageSize).then(data=>callBack(data));
    }, []);
    
    function callBack(data){
        setIsLoading(false);
        console.log("in callback", data.data);
        setTotalItems(data.data.totalItems);
        setAdsData(data.data.items);
    }

    const handleChange = event => {
        setViewType(event.target.value);
    };

    async function handlePageChange(event, page) {
        console.log("page", page, "pageNumber", pageNumber);
        setPage(page);
        const returnedData = await getXAds(page, pageSize);
        console.log("inside handlePageChange", returnedData.data);
        // why the following is necessary ? I don't understand ?
        setAdsData([]);
        setAdsData([...returnedData.data.items]);
    }

    const sectionTwo  = {
        display:"flex",
        justifyContent:"space-between",
        padding:"10px"
    }
    
    const loader = {
        position :"fixed",
        top:"50%",
        right:"50%"
    }

    return (
        <div>
            {isLoading ? (
                <h1 style={loader}>Loading data </h1>
            ) : (
                <div>
                    <div style={sectionTwo}>
                        <Select native value={viewType} onChange={handleChange}>
                            <option value={"card"}>Card</option>
                            <option value={"list"}>List</option>
                        </Select>
                        <Pagination
                            // get total number of ads from the backend 
                            count={Math.ceil(totalItems/pageSize)}
                            page={pageNumber}
                            onChange={handlePageChange}
                        />

                           <TextField id="standard-basic" label="search" />

                    </div>
                    <div>
                        {viewType === "card" ? (
                            <AdsCart adsData={adsData} viewType={viewType} />
                        ) : (
                            <AdsList adsData={adsData} viewType={viewType} />
                        )}
                    </div>
                </div>
            )}
        </div>
    );
};
export default DisplayAllAds;
