
import React from "react";

import { FaBeer } from "react-icons/fa";
import { FaReply } from "react-icons/fa";
import { FaShareAlt } from "react-icons/fa";
import { FaFacebookMessenger } from "react-icons/fa";

function ControlItems() {

    const controlItem = {
        display: "flex",
        padding: "0px 5px 5px 0px",
        alignItems: "baseline",
        justifyContent: "flex-start",
        textAlign: "flex-start"
    };

    const controlItems ={
        flexBasis: "10%"
    }
    return (
        <div style={controlItem}>
            <FaBeer style={controlItems}/>
            <FaReply style ={controlItems}/>
            <FaShareAlt style={controlItems}/>
            <FaFacebookMessenger style={controlItems}/>
        </div>
    );
}
function RoomSummary(props) {
    const summaryStyle = {
        display: "flex",
        direction: "row",
        flexWrap: "true",
        alignItems: "baseline",
        justifyContent: "flex-start",
        padding: "0 5px",
        flexGrow: 2,
        flex: "1 auto 1"
    };

    const textStyle = {
        display: "inline",
        width: "max-content",
        flexGrow: "2"
    };

    const {price,title,date} = props
    //console.log(props);

    return (
        <div style={summaryStyle}>
            <p>{date}</p>
            <p>{title}</p>
            <p>{price}</p>
        </div>
    );
}

function RoomHandle(props) {
    const roomHandleStyle = {
        backgroundColor: "#002366",
        color: "white",
        /*width: "20%",*/
        padding: "5px"
    };
    const {price, title} = props;
    return (
        <div style={roomHandleStyle}>
            <RoomSummary  price={price} title={title}/>
            <ControlItems />
            <p>
                This is a great romo asdfasdf adsfa dfad as df a dfadfsad fa f a
                fsdf a dsf a{" "}
            </p>
        </div>
    );
}

export default RoomHandle;
