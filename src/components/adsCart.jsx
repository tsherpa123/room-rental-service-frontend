import React from "react";
import PropTypes from "prop-types";
import Ad from "./ad.jsx";

const adsContainterStyle = {
    display: "flex",
    flexWrap: "wrap"
};

function AdsCart(props) {
    const { adsData, viewType } = props;
    console.log("client ads",adsData);
    return (
        <div style={adsContainterStyle} className={"col-sm-12"}>
            
            {adsData.map(x => (
                <Ad adData={x} adViewType={viewType} />
            ))}
            
        </div>
    );
}

AdsCart.defaultProps = {};

AdsCart.propTypes = {};

export default AdsCart;
