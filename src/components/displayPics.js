import React from "react";
import { useState } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
//import RoomHandle from "./roomHandle";
// an ad renderer
function DisplayPics(props) {
    const [pics, setPics] = useState(props.pictures);
    const styles = {
        height: "200px",
        width: "100%"
    };

    if (!Array.isArray(pics)) {
        const listStyles ={
            height:"100%",
            width:"305"
        }
        return (
            <div style={listStyles}>
                <img alt="room" style={styles} src={process.env.PUBLIC_URL + pics} />
            </div>
        );
    }

    return (
        <div>
            <Carousel
                showArrows={true}
                showStatus={true}
                showIndicators={true}
                autoPlay={false}
                swipeable={true}
                showThumbs={false}
            >
                {pics.map(pic => (
                    <div style={styles}>
                        <img
                            alt ="room"
                            style={styles}
                            src={process.env.PUBLIC_URL + pic}
                        />
                    </div>
                ))}
            </Carousel>
        </div>
    );
}

export default DisplayPics;
