import React from 'react';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
const SayHelloWorld = function sayHello(){
    return (
        <div>
            <h1>Hello</h1>
        </div>

    );
}
        
const SayWorld = function sayWorld(){
    return (
        <div>
            <h2>World</h2>
        </div>
    );
}

export {SayHelloWorld,App,SayWorld};

