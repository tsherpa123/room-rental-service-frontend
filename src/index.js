import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
//import {App} from './App';
import "bootstrap/dist/css/bootstrap.min.css";
import { Filter, Filters } from "./components/filter.jsx";
import DisplayAllAds from "./components/ads.jsx";
import * as serviceWorker from "./services/serviceWorker.js";

const sectionOne = {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "darkgrey"
};

const loginInfo ={
    display: "flex"
}

const margin = {
    marginRight :"10px"
}

ReactDOM.render(
    <React.StrictMode>
        <span style={sectionOne}>
            <Filters />
            <div style={loginInfo}>
                <h3 style={margin}>post</h3>
                <h3>account</h3>
            </div>
        </span>
        <DisplayAllAds />
    </React.StrictMode>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
