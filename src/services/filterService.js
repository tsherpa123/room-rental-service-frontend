// this file contains the service required by the Filters component

export let categoryMap = new Map();
// this will be actually database call, with condition where the parents are null
categoryMap.set("rootnode", ["abc", "def", "ghra"]);
categoryMap.set("abc", ["hij", "lkm", "mno"]);
categoryMap.set("def", ["1", "2", "3"]);
categoryMap.set("ghra", ["123", "456", "789"]);
categoryMap.set("789", ["qwer", "asdfasdf", "asdfasdfadf"]);

/**
 * This is a mock function will be changed to an API call later
 */
export function getChildren(key) {
    if (categoryMap.has(key)) {
        return categoryMap.get(key);
    }
    return [];
}

/**
 * This is a mock function to get the parent
 * will/should be implemented as a db endpoint call
 */
export function getParent(child) {
    if (["hij", "lkm", "mno"].includes(child)) {
        return "abc";
    } else if (["1", "2", "3"].includes(child)) {
        return "def";
    } else if (["123", "456", "789"].includes(child)) {
        return "ghra";
    }
     else if (["abc", "def", "ghra"].includes(child)) {
        return "rootnode";
    }
    return null;
}
