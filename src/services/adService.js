// post utility functions
// this service is for serving ads

/**
 * method to get all the ads from the backend service
 * @return returns all the ads
 */
const baseUri = "http://localhost:3000/ads";
export async function getAllAds(adsEndpoint) {
    try {
        console.log("fethching all ads");
        const data = await fetch(adsEndpoint);
        const parsedJson = await data.json();
        return parsedJson;
    } catch (error) {
        console.log("error while fetching all ads");
        return error;
    }
}

/**
 * method to get a particular ad for the provided post
 * this will return ad not available if the ad is not present
 * 
 */

export  async function getOneAd(adId) {
    const url = `${baseUri}/:${adId}`;
    const data =  await fetch(url);
    return  await data.json();
}

/*
 * function to get all the ad details
 * takes a valid ad id
 */

export  async function getAdDetails(adId) {
    const url = `${baseUri}/:${adId}`;
    const data =  await fetch(url);
    return  await data.json();
}

/**
 * this method is used for getting x number of ads
 * it can be used for pagination purposes later
 * x>0 less than total ads/pagination size
 */

export async function getXAds(pageNumber, pageSize) {
    try {
        console.log(`fetching ${pageSize} ads`);
        const data = await fetch(
            // http :3001/ads/adsList/listAll pageNumber==1 pageItemCount==10
            `http://localhost:3001/ads/adsList/listAll?pageNumber=${pageNumber}&pageItemCount=${pageSize}`
        );
        const dataParsed = await data.json();
        console.log(dataParsed.data);
        return dataParsed;
    } catch (error) {
        console.log(error);
        console.log("there was an internal service issue");
    }
}
/**
 * This method will return the ads/posts that satify the filter requirements
 * The filter requirements are passed in as function parameters
 */


